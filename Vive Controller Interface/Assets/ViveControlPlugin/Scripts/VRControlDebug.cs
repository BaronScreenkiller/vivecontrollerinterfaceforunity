﻿using UnityEngine;
using System.Collections;

public class VRControlDebug : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        ViveInput.GripButtonDown += GripButtonDown;
        ViveInput.GripButtonStay += GripButtonStay;
        ViveInput.GripButtonUp += GripButtonUp;
    }

    void GripButtonDown(int index)
    {
        Debug.Log("Grip Down " + index);
    }

    void GripButtonStay(int index)
    {
        Debug.Log("Grip Stay");
    }

    void GripButtonUp(int index)
    {
        Debug.Log("Grip Up");
    }
}
