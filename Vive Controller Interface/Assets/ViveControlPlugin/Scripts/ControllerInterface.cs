﻿using UnityEngine;
using System.Collections;
/// by Peter Mills
/// peter.mills@heliumdreams.com.au
/// 
/// Used By ViveInput to Create Events
/// Can have custom scripts built directly on top of it

/// <summary>
/// This Class Attaches to each Vive controller and accesses its input status exposing the information as public variables
/// </summary>
public class ControllerInterface : MonoBehaviour
{
    //Ensures that controller is equal to the Controller associate with this scripts tracked object each time it is accessed
    //(Provides protection from index numbers being changed as devices are connected and disconnected)
    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    //The tracked object attached to this game object (should be a controller)
    private SteamVR_TrackedObject trackedObj;

    //Grip button information
    //Reference to the correct button ID
    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    //Variables required to express this buttons current state
    public bool gripButtonDown = false;
    public bool gripButtonUp = false;
    public bool gripButtonStay = false;

    //Touchpad Information
    //For Press and Touch
    private Valve.VR.EVRButtonId touchPad = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
    /// <summary>
    /// True when the touch pad button is pressed
    /// </summary>
    public bool touchPadPressDown = false;
    public bool touchPadPressUp = false;
    public bool touchPadPressStay = false;
    public bool touchPadTouchDown = false;
    public bool touchPadTouchUp = false;
    public bool touchPadTouchStay = false;
    public Vector2 touchPadAxis = Vector2.zero;

    //Trigger Information
    //For Full Depression and Hair trigger
    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    public bool triggerButtonDown = false;
    public bool triggerButtonUp = false;
    public bool triggerButtonStay = false;

    [Range(0.0f, 1.0f), Tooltip("Range: 0 - 1 used to set hair trigger threshold")]
    /// <summary>
    /// Range: 0 - 1 used to set hair trigger threshold
    /// </summary>
    public float hairTriggerDelta = 0.1f;
    public bool hairTriggerDown = false;
    public bool hairTriggerUp = false;
    public bool hairTriggerStay = false;
    public float hairTriggerAmount = 0.0f;


    private Valve.VR.EVRButtonId systemButton = Valve.VR.EVRButtonId.k_EButton_System;
    public bool systemButtonDown = false;
    public bool systemButtonUp = false;
    public bool systemButtonStay = false;

    private Valve.VR.EVRButtonId applicationMenuButton = Valve.VR.EVRButtonId.k_EButton_ApplicationMenu;
    public bool applicationMenuButtonDown = false;
    public bool applicationMenuButtonUp = false;
    public bool applicationMenuButtonStay = false;


    // Use this for initialization
    void Start ()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();        
    }

    // Update is called once per frame
    void Update ()
    {
        if (controller == null)
        {
            Debug.Log("Controller Not Initialized");
        }
        else
        {
            UpdateControlStates();
        }                
    }

    void UpdateControlStates()
    {
        //Grip Button Bools
        gripButtonDown = controller.GetPressDown(gripButton);
        gripButtonUp = controller.GetPressUp(gripButton);
        gripButtonStay = controller.GetPress(gripButton);

        //Touchpad button Bools
        touchPadPressDown = controller.GetPressDown(touchPad);
        touchPadPressUp = controller.GetPressUp(touchPad);
        touchPadPressStay = controller.GetPress(touchPad);
        //Touchpad touch bools and touch axix
        //If touch is wonky, recalibrate controllers.
        //Turn off --> Hold System Button and Trigger all the way in --> turn back on
        //https://developer.valvesoftware.com/wiki/SteamVR/TroubleshootingFirstGen#Controllers
        touchPadTouchDown = controller.GetTouchDown(touchPad);
        touchPadTouchUp = controller.GetTouchUp(touchPad);
        touchPadTouchStay = controller.GetTouch(touchPad);
        touchPadAxis = controller.GetAxis(touchPad);
        //Trigger Bools
        //for full trigger depression
        triggerButtonStay = controller.GetPress(triggerButton);
        triggerButtonDown = controller.GetPressDown(triggerButton);
        triggerButtonUp = controller.GetPressUp(triggerButton);
        //Hair trigger bools
        //True when trigger moves past Hair trigger delta
        //0 is almost not pressed at all
        //1 is pressed harder then the buttons
        hairTriggerStay = controller.GetHairTrigger();
        hairTriggerDown = controller.GetHairTriggerDown();
        hairTriggerUp = controller.GetHairTriggerUp();
        hairTriggerAmount = controller.GetState().rAxis1.x;
        if(controller.hairTriggerDelta != hairTriggerDelta)
        {
            controller.hairTriggerDelta = hairTriggerDelta;
        }

        //Bools for System Button
        systemButtonDown = controller.GetPressDown(systemButton);
        systemButtonUp = controller.GetPressUp(systemButton);
        systemButtonStay = controller.GetPress(systemButton);

        //Bools for Menu Button
        applicationMenuButtonDown = controller.GetPressDown(applicationMenuButton);
        applicationMenuButtonUp = controller.GetPressUp(applicationMenuButton);
        applicationMenuButtonStay = controller.GetPress(applicationMenuButton);
    }
}
