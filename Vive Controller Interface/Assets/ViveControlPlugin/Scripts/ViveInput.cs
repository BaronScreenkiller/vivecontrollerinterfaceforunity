﻿using UnityEngine;
using System.Collections;
/// <summary>
/// This Class Keeps an array of ControllerInterfaces and exposes their information through static methods
/// by Peter Mills
/// peter.mills@heliumdreams.com.au
/// </summary>
public class ViveInput : MonoBehaviour
{
    //TODO: Replace with list, have ControllerInterfaces add themselves
    ControllerInterface[] controllers;

    public static event VRControlPressed GripButtonDown, GripButtonUp, GripButtonStay;

    public static event VRControlPressed MenuButtonDown, MenuButtonUp, MenuButtonStay;

    public static event VRControlPressed TriggerDown, TriggerUp, TriggerStay;
    public static event VRControlPressed HairTriggerDown, HairTriggerUp, HairTriggerStay;
    public static event VRTriggerValue HairTriggerValue;
    public static event VRSetHairTriggerThreshold SetHairTriggerThreshold;

    public static event VRControlPressed TouchPadPressDown, TouchPadPressUp, TouchPadPressStay;
    public static event VRControlPressed TouchPadTouchDown, TouchPadTouchUp, TouchPadTouchStay;
    public static event VRTouchPadValue TouchPadValue;

    public static event VRGetControllerTransform GetControllerTransform;

    // Use this for initialization
    void Start ()
    {
        controllers = GetComponentsInChildren<ControllerInterface>();

        SetHairTriggerThreshold += HairTriggerThreshold;
        GetControllerTransform += PassControllerTransform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //This Check is run every frame to ensure connected and reconnected controllers are added;
        controllers = GetComponentsInChildren<ControllerInterface>();

        //Loop through each method can call each event as required, passing in the controller index
        for (int i = 0; i < controllers.Length; i++)
        {
            //*************   Grip Buttons   *************//
            if (controllers[i].gripButtonDown)
            {
                if (GripButtonDown != null) { GripButtonDown(i); }
            }
            if(controllers[i].gripButtonStay)
            {
                if (GripButtonStay != null) { GripButtonStay(i); }
            }
            if (controllers[i].gripButtonUp)
            {
                if (GripButtonUp != null) { GripButtonUp(i); }
            }
            //*************   Menu Buttons   *************//
            if (controllers[i].applicationMenuButtonDown)
            {
                if(MenuButtonDown != null) { MenuButtonDown(i); }
            }

            if (controllers[i].applicationMenuButtonUp)
            {
                if (MenuButtonUp != null) { MenuButtonUp(i); }
            }

            if (controllers[i].applicationMenuButtonStay)
            {
                if (MenuButtonStay != null) { MenuButtonStay(i); }
            }
            //*************     Triggers      ************//
            //Trigger Buttons//
            if (controllers[i].triggerButtonDown)
            {
                if(TriggerDown != null) { TriggerDown(i); }
            }
            if (controllers[i].triggerButtonUp)
            {
                if (TriggerUp != null) { TriggerUp(i); }
            }
            if (controllers[i].triggerButtonStay)
            {
                if (TriggerStay != null) { TriggerStay(i); }
            }
            //Hair Trigger//
            if (controllers[i].hairTriggerDown)
            {
                if (HairTriggerDown != null) { TriggerDown(i); }
            }
            if (controllers[i].hairTriggerUp)
            {
                if (HairTriggerUp != null) { HairTriggerUp(i); }
            }
            if (controllers[i].hairTriggerStay)
            {
                if (HairTriggerStay != null) { HairTriggerStay(i); }
            }
            if(HairTriggerValue != null) { HairTriggerValue(i, controllers[i].hairTriggerAmount); }

            //*************    Touch Pad     *************//
            //Touch Pad Pressed
            if (controllers[i].touchPadPressDown)
            {
                if (TouchPadPressDown != null) { TouchPadPressDown(i); }
            }
            if (controllers[i].touchPadPressUp)
            {
                if (TouchPadPressUp != null) { TouchPadPressUp(i); }
            }
            if (controllers[i].touchPadPressStay)
            {
                if (TouchPadPressStay != null) { TouchPadPressStay(i); }
            }
            //Touch Pad Touched
            if (controllers[i].touchPadTouchDown)
            {
                if (TouchPadTouchDown != null) { TouchPadTouchDown(i); }
            }
            if (controllers[i].touchPadTouchUp)
            {
                if (TouchPadTouchUp != null) { TouchPadTouchUp(i); }
            }
            if (controllers[i].touchPadTouchStay)
            {
                if (TouchPadTouchStay != null) { TouchPadTouchStay(i); }
            }
            if(TouchPadValue != null) { TouchPadValue(i, controllers[i].touchPadAxis); }
        }
    }

    //Sets the hair trigger value
    //New values clamped between 0 and 1
    void HairTriggerThreshold(int index, float amount)
    {
        amount = amount > 1 ? 1 : amount;
        amount = amount < 0 ? 0 : amount;

        controllers[index].hairTriggerDelta = amount;
    }

    Transform PassControllerTransform(int index)
    {
        if(index < controllers.Length)
        {
            return controllers[index].transform;
        }
        else
        {
            return null;
        }
    }
}
//Events Triggered by input
public delegate void VRControlPressed(int controllerIndex);
public delegate void VRTriggerValue(int controllerIndex, float value);
public delegate void VRTouchPadValue(int controllerIndex, Vector2 value);
//Events Triggered by other scripts
public delegate void VRSetHairTriggerThreshold(int controllerIndex, float value);
public delegate Transform VRGetControllerTransform(int controllerIndex);

public enum VRButton { Trigger, Grip, Touchpad, Menu };